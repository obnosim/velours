# Theme for KDE Plasma 5
![Velours in Plasma 5.27.5](https://i.imgur.com/4yfwdi2.png "Velours in Plasma 5.27.5")
A dark plasma theme with a fuzzy texture and monochromatic icons, designed to be easy on the eyes, simple and usable.
It draws inspiration from Caledonia by Malcer (which it was originally a fork of) and openSUSE by sumski.

To install this theme, either clone this repository or extract a release in ~/.local/share/plasma/desktoptheme/ for a local user or globally in /usr/share/plasma/desktoptheme/ .
